Currently using Foundation 6.2.1

### What is this?
excelerator is a blank WordPress theme built on top of JointsWP - which is in turn built with Foundation 6, giving the developers of the excelerator theme the power and flexibility you need to build complex, mobile friendly websites without having to start from scratch.