<?php
// SIDEBARS AND WIDGETIZED AREAS
function excelerator_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __('Sidebar 1', 'excelerator'),
		'description' => __('The first (primary) sidebar.', 'excelerator'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	register_sidebar(array(
		'id' => 'offcanvas',
		'name' => __('Offcanvas', 'excelerator'),
		'description' => __('The offcanvas sidebar.', 'excelerator'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

		register_sidebar(array(
			'id' => 'footer-1',
			'name' => __('Footer 1', 'excelerator'),
			'description' => __('Footer widget', 'excelerator'),
			'before_widget' => '<div id="%1$s" class="footer-widget-inner %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="footer-widget__title">',
			'after_title' => '</h4>',
		));

		register_sidebar(array(
			'id' => 'footer-2',
			'name' => __('Footer 2', 'excelerator'),
			'description' => __('Footer widget', 'excelerator'),
			'before_widget' => '<div id="%1$s" class="footer-widget-inner %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="footer-widget__title">',
			'after_title' => '</h4>',
		));

		register_sidebar(array(
			'id' => 'footer-3',
			'name' => __('Footer 3', 'excelerator'),
			'description' => __('Footer widget', 'excelerator'),
			'before_widget' => '<div id="%1$s" class="footer-widget-inner %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="footer-widget__title">',
			'after_title' => '</h4>',
		));

		register_sidebar(array(
			'id' => 'footer-4',
			'name' => __('Footer 4', 'excelerator'),
			'description' => __('Footer widget', 'excelerator'),
			'before_widget' => '<div id="%1$s" class="footer-widget-inner %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="footer-widget__title">',
			'after_title' => '</h4>',
		));

	/*
	to add more sidebars or widgetized areas, just copy
	and edit the above sidebar code. In order to call
	your new sidebar just use the following code:

	Just change the name to whatever your new
	sidebar's id is, for example:

	register_sidebar(array(
		'id' => 'sidebar2',
		'name' => __('Sidebar 2', 'excelerator'),
		'description' => __('The second (secondary) sidebar.', 'excelerator'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	To call the sidebar in your template, you can just copy
	the sidebar.php file and rename it to your sidebar's name.
	So using the above example, it would be:
	sidebar-sidebar2.php

	*/
} // don't remove this bracket!
