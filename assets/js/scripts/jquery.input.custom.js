/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/

(function() {
	'use strict';
}());

;( function( $, window, document, undefined )
{
	$( '.wpcf7-file' ).each( function()
	{
		var $input	 = $( this ),
			$label	 = $input.parent('span').next( 'label' ),
			labelVal = $label.html();

		$input.on( 'change', function( e )
		{
			var fileName = '';

			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else if( e.target.value )
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				$label.find( 'span' ).html( fileName );
			else
				$label.html( labelVal );
		});

		// Firefox bug fix
		$input
		.on( 'focus', function(){ $input.parent().addClass( 'has-focus' ); })
		.on( 'blur', function(){ $input.parent().removeClass( 'has-focus' ); });
	});
})( jQuery, window, document );
