jQuery(document).foundation();

/*
These functions make sure WordPress
and Foundation play nice together.
*/

function handleScroll() {
	var previousScroll = pageYOffset;

	jQuery(window).scroll(function(){
		var currentScroll = jQuery(this).scrollTop();
		if (currentScroll > previousScroll){
			//console.log('down');
	 	} else {
			//console.log('up');
		}
		previousScroll = currentScroll;
		if ( pageYOffset < 100 ) {
			jQuery('a.arrow').hide();
		} else {
			jQuery('a.arrow').show();
		}

	});
}


jQuery(document).ready(function() {

	// Trigger input click when label's clicked.
	jQuery('label').click(function() {
		console.log('label clicked');
		labelID = jQuery(this).attr('for');
		var $fileinput = jQuery('.'+labelID).find('input');
		console.log($fileinput);
		$fileinput.trigger('click');
	});

    // Remove empty P tags created by WP inside of Accordion and Orbit
    jQuery('.accordion p:empty, .orbit p:empty').remove();

	 // Makes sure last grid item floats left
	jQuery('.archive-grid .columns').last().addClass( 'end' );

	// Adds Flex Video to YouTube and Vimeo Embeds
  jQuery('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').each(function() {
    if ( jQuery(this).innerWidth() / jQuery(this).innerHeight() > 1.5 ) {
      jQuery(this).wrap("<div class='widescreen flex-video'/>");
    } else {
      jQuery(this).wrap("<div class='flex-video'/>");
    }
  });

  jQuery('#excelerator-body').on('scrollme.zf.trigger',handleScroll);

});

/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/

(function() {
	'use strict';
}());

;( function( $, window, document, undefined )
{
	$( '.wpcf7-file' ).each( function()
	{
		var $input	 = $( this ),
			$label	 = $input.parent('span').next( 'label' ),
			labelVal = $label.html();

		$input.on( 'change', function( e )
		{
			var fileName = '';

			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else if( e.target.value )
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				$label.find( 'span' ).html( fileName );
			else
				$label.html( labelVal );
		});

		// Firefox bug fix
		$input
		.on( 'focus', function(){ $input.parent().addClass( 'has-focus' ); })
		.on( 'blur', function(){ $input.parent().removeClass( 'has-focus' ); });
	});
})( jQuery, window, document );
