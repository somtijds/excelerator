<?php
/**
 * Footer Content Template
 * Author: Willem Prins | SOMTIJDS
 * Project: Excelerator
 * Date created: 05/08/2016
 *
 * @package Exchange Plugin
 **/
// Exit if accessed directly.
?>
<div class="footer-widgets-wrapper">
	<?php if ( is_active_sidebar('footer-1' ) ) : ?>
		<div class="footer-widget columns small-12 medium-4">
			<?php dynamic_sidebar( 'footer-1' ); ?>
		</div>
	<?php endif; ?>
	<?php if ( is_active_sidebar('footer-2' ) ) : ?>
		<div class="footer-widget columns small-12 medium-8">
			<?php dynamic_sidebar( 'footer-2' ); ?>
		</div>
	<?php endif; ?>
	<?php if ( is_active_sidebar('footer-3' ) ) : ?>
		<div class="footer__widget-wrapper">
			<?php dynamic_sidebar( 'footer-3' ); ?>
		</div>
	<?php endif; ?>
	<?php if ( is_active_sidebar('footer-4' ) ) : ?>
		<div class="footer__widget-wrapper">
			<?php dynamic_sidebar( 'footer-4' ); ?>
		</div>
	<?php endif; ?>
</div>
