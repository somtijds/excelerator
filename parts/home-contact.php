<!-- CONTACT --->
<section id="contact" class="contact">
	<div class="section-inner">
	<?php $contact_header = get_field( 'contact_header' );
		if ( ! empty( $contact_header ) ) : ?>
		<header class="section_header row column">
			<h2><?php echo $contact_header; ?></h2>
		</header>
		<?php endif;
		if ( ! empty( $contact_form ) ) : ?>
		<div class="items-wrapper row">
			<div class="section-item column">
				<?php echo $contact_form; ?>
			</div>
		</div>
		<?php endif; ?>
	</div><!-- section-inner -->
</section><!-- CONTACT -->
