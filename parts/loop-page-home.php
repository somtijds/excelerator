<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">

	<?php
		$intro_text = get_field( 'intro');
		if ( ! empty( $intro_text ) ) {
			include_once( locate_template( 'parts/home-intro.php' ) );
		}
		if ( have_rows( 'prijzen' ) ) {
			get_template_part( 'parts/home', 'prijzen' );
		}
		if ( have_rows( 'modules' ) ) {
			get_template_part( 'parts/home', 'modules' );
		}
		$contact_form = get_field( 'contact_form' );
		if ( ! empty( $contact_form ) ) {
			include_once( locate_template( 'parts/home-contact.php' ) );
		}
	?>

</article> <!-- end article -->
