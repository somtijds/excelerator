<div class="top-bar" id="main-menu">
	<div class="top-bar-left">
		<div class="logo"><?php echo get_bloginfo('name') ?></div>
		<div class="filter"><svg class="filter-image" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 36.358 30.485"><path d="M18.203.004c5.714 0 11.428.001 17.141-.004.418 0 .775.09.945.503.175.424-.002.755-.313 1.061-4.427 4.36-8.848 8.726-13.279 13.082-.285.28-.417.552-.414.963.019 3.318.002 6.636.022 9.954.004.612-.213.987-.753 1.286-2.025 1.121-4.028 2.282-6.04 3.427-.831.473-1.444.124-1.445-.835-.005-4.663-.005-9.326.001-13.99 0-.277-.073-.474-.274-.673C9.321 10.375 4.854 5.966.385 1.557.019 1.196-.088.854.071.497.255.084.608.001 1.022.001 6.75.006 12.476.004 18.203.004z"></path><image src="<?php echo get_template_directory_uri(); ?>/assets/images/icon_filter.png" xlink:href=""></svg></div>
	</div>
	<div class="top-bar-right">
		<?php excelerator_top_nav(); ?>
	</div>
</div>
