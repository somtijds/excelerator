<!-- SECTIE INTRO -->
<header class="grey top article-header" id="intro">
	<div class="header-inner">
		<!-- INTRO -->
		<div class="intro-wrapper">
		<?php
			if ( ! empty( $intro_header ) ) : ?>
			<header class="section_header row">
				<h2 class="intro_header">
					<?php echo $intro_header; ?>
				</h2>
			</header>
		<?php
			endif;
			if ( ! empty( $intro_text ) ) : ?>
				<div class="intro_content row">
					<div class="column small-centered large-uncentered medium-11 end">
						<?php echo $intro_text; ?>
					</div>
				</div>
			<?php endif; ?>

			<?php
				$acf_button_text = get_field( 'excelerate_button' );
				$button_text = empty( $acf_button_text ) ? __('Excelerate','excelerate') : $acf_button_text;
				if ( $button_text ) : ?>
				<div class="intro_button row">
					<div class="column small-centered">
						<a href="#contact" class="button smoothScroll"><?php echo $button_text; ?></a>
					</div>
				</div>

			<?php endif; ?>

		</div>
		<!-- PORTRET -->
		<?php $intro_image = get_the_post_thumbnail( $post->ID, 'large', array( 'class' => 'foto' ) );
			if ( ! empty( $intro_image ) ) : ?>
			<div class="foto-wrapper small-12 medium-4 small-centered ">
				<?php echo $intro_image; ?>
			</div><!--foto-wrapper-->
			<?php endif; ?>

	</div><!--header-inner-->
</header>
