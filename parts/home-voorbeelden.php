<!-- VOORBEELDEN --->
<section class="voorbeelden grey">
	<div class="section-inner">
	<?php $voorbeelden_header = get_field( 'voorbeelden_header' );
		if ( ! empty( $voorbeelden_header ) ) : ?>
		<header class="section_header row column">
			<h2><?php echo $voorbeelden_header; ?></h2>
		</header>
	<?php endif; ?>
	<div class="items-wrapper row">

		<?php while ( have_rows( 'voorbeelden' ) ) : the_row(); ?>

		<?php
			$acf_niveau_label = get_sub_field( 'niveau_label' );
			$niveau_label = empty( $acf_niveau_label ) ? __('Niveau:','excelerator' ) : $acf_niveau_label;
			$acf_prijs_label = get_sub_field( 'prijs_label' );
			$prijs_label = empty( $acf_prijs_label ) ? __('Prijs:','excelerator' ) : $acf_prijs_label;
			$voorbeeld = array(
				'titel'         => get_sub_field( 'titel' ),
				'niveau_label'  => $niveau_label,
				'niveau'        => get_sub_field( 'niveau' ),
				'prijs_label'   => $prijs_label,
				'prijs'         => get_sub_field( 'prijs' ),
				'beschrijving'  => get_sub_field( 'beschrijving' ),
			);
			// If there is no description or no title, move to the next row.
			if ( empty( $voorbeeld['titel'] ) || empty( $voorbeeld['beschrijving'] ) ) {
				continue;
			} ?>
			<div class="section-item voorbeeld columns small-12 medium-6">
				<h3 class="item-header"><?php echo $voorbeeld['titel'] ?></h3>

			<?php if ( ! empty( $voorbeeld['prijs' ] ) || ! empty( $voorbeeld['niveau'] ) ) : ?>
				<table class="ondertitel">
					<?php /* if ( ! empty( $voorbeeld['niveau'] ) ) : ?>
						<tr>
							<td class="tabel_label"><?php echo $voorbeeld['niveau_label']; ?></td>
							<td class="waarde"><?php echo $voorbeeld['niveau']; ?></td>
						</tr>
					<?php endif; */ ?>
					<?php if ( ! empty( $voorbeeld['prijs'] ) ) : ?>
						<tr>
							<td class="tabel_label"><?php echo $voorbeeld['prijs_label']; ?></td>
							<td class="waarde">&euro; <?php echo $voorbeeld['prijs']; ?></td>
						</tr>
					<?php endif; ?>
				</table>
			<?php endif; ?>
				<p class="item-description'"><?php echo $voorbeeld['beschrijving']; ?></p>
			</div><!-- voorbeeld -->

			<?php endwhile; ?>

		</div><!-- items-wrapper -->
	</div><!-- section-inner -->
</section><!-- VOORBEELDEN -->
