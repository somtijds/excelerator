<!-- SECTIE modules -->
<section class="modules grey">
	<div class="section-inner">

	<?php $modules_header = get_field( 'modules_header' );
		$first_slide = ' is-active ';
		if ( ! empty( $modules_header ) ) : ?>
		<header class="section_header row column">
			<h2><?php echo $modules_header; ?></h2>
		</header>

	<?php endif; ?>

		<div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit data-auto-play="false">
	  		<ul class="orbit-container">

				<button class="orbit-previous"><span class="show-for-sr">Previous Slide</span><?php echo excelerator_build_svg( get_template_directory() . '/assets/images/arrow_left.svg', true ); ?></button>
				<button class="orbit-next"><span class="show-for-sr">Next Slide</span><?php echo excelerator_build_svg( get_template_directory() . '/assets/images/arrow_right.svg', true ); ?></button>

			<?php while ( have_rows( 'modules' ) ) : the_row(); ?>
			<?php
				$module = array(
					'titel'         => get_sub_field( 'module_naam' ),
					'prijs'         => get_sub_field( 'module_prijs' ),
					'beschrijving'  => get_sub_field( 'module_beschrijving' ),
					'afbeelding'    => get_sub_field( 'module_afbeelding' ),
				);
				// If there is no description, price, image or no title, move to the next row.
				if ( empty( $module['titel'] )
					|| empty( $module['prijs'] )
					|| empty( $module['beschrijving'] )
					|| empty( $module['afbeelding'] )
					) {
					continue;
				}
				?>
					<li class="orbit-slide module <?php echo $first_slide; ?>">
						<div class="row">
							<div class="module_content module_text columns small-12 medium-6">
								<div class="module_image_icon-wrapper hide-for-medium">
									<img
										src="<?php echo esc_attr( $module['afbeelding'] ); ?>"
										alt="<?php echo esc_attr( $module['titel'] ); ?>"
										title="<?php echo esc_attr( $module['titel'] ); ?>">
								</div>
								<header class="item-header">
									<h4><?php echo $module['titel'] ?></h4>
									<p class="ondertitel"><?php echo '&euro;' . $module['prijs'] . __( ',-', 'excelerator' ); ?></p>
								</header>
								<div class="item-description">
									<?php echo $module['beschrijving']; ?></p>
								</div>
							</div>
							<div class="module_content module_image columns small-12 hide-for-small-only medium-6">
								<div class="module_image-wrapper">
								<div class="center-me">
									<img
										src="<?php echo esc_attr( $module['afbeelding'] ); ?>"
										alt="<?php echo esc_attr( $module['titel'] ); ?>"
										title="<?php echo esc_attr( $module['titel'] ); ?>">
									</div>
								</div>
							</div>
						</div>
					</li>
				<?php $first_slide = ''; ?>
				<?php endwhile; ?>

			</ul><!-- orbit-container -->
		</div><!-- orbit -->
	</div><!-- section-inner -->
</section>
