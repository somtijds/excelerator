<!-- SECTIE PRIJZEN -->
<section class="prijzen white">
	<div class="section-inner">

	<?php $prijzen_header = get_field( 'prijzen_header' );
	$prijzen_header = get_field( 'prijzen_header' );
	$prijzen_intro = get_field( 'prijzen_intro' );
		if ( ! empty( $prijzen_header ) ) : ?>
		<header class="section_header row column">
			<h2><?php echo $prijzen_header; ?></h2>
		</header>
	<?php endif;
		if ( ! empty( $prijzen_intro ) ) : ?>
			<div class="prijzen_content row">
				<div class="column small-centered large-uncentered medium-11 end">
					<?php echo $prijzen_intro; ?>
				</div>
			</div>
		<?php endif; ?>
	</div><!-- section-inner -->
</section>
