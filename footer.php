		<a title="<?php _e('Terug naar boven','excelerator'); ?>" href="#top-nav" class="arrow smoothScroll"><?php echo excelerator_build_svg( get_template_directory() . '/assets/images/arrow_up.svg', true ); ?></a>

		<footer class="widgets-footer" role="contentinfo">
			<div class="footer-inner row">
				<?php get_template_part( 'parts/content', 'footer' ); ?>
			</div>
			<div class="footer-inner row">
				<div class="content-info medium-12 columns">
					<?php $terms_upload = get_field( 'algemene_voorwaarden_upload', get_option('page_on_front') ); ?>
					<p class="source-org copyright">
						&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>
						<?php if ( ! empty( $terms_upload ) ) : ?>
							| <a href="<?php echo $terms_upload; ?>" title="<?php _e(' Download de algemene voorwaarden', 'excelerator'); ?>"><?php _e('algemene voorwaarden','excelerator'); ?></a>
						<?php endif; ?>
					</p>
				</div>
			</div> <!-- end #inner-footer -->
		</footer> <!-- end .footer -->
		<?php wp_footer(); ?>
		<script>
		(function( w ){
		if( w.document.documentElement.className.indexOf( "fonts-loaded" ) > -1 ){
			return;
		}
		var font1 = new w.FontFaceObserver( "Oxygen", {
		    weight: 400
		});
		var font2 = new w.FontFaceObserver( "Oxygen", {
		    weight: 700,
		    style: "italic"
		});

		w.Promise
		    .all([
				font1.check(),
				font2.check(),
			]).then(function(){
		        w.document.documentElement.className += " fonts-loaded";
		    });
		}( this ));
		</script>
	</body>
</html> <!-- end page -->
