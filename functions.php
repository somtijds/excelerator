<?php
// Theme support options
require_once(get_template_directory().'/assets/functions/theme-support.php');

// WP Head and other cleanup functions
require_once(get_template_directory().'/assets/functions/cleanup.php');

// Register scripts and stylesheets
require_once(get_template_directory().'/assets/functions/enqueue-scripts.php');

// Register custom menus and menu walkers
require_once(get_template_directory().'/assets/functions/menu.php');

// Register sidebars/widget areas
require_once(get_template_directory().'/assets/functions/sidebar.php');

// Makes WordPress comments suck less
require_once(get_template_directory().'/assets/functions/comments.php');

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory().'/assets/functions/page-navi.php');

// Adds support for multiple languages
require_once(get_template_directory().'/assets/translation/translation.php');

// Remove 4.2 Emoji Support
// require_once(get_template_directory().'/assets/functions/disable-emoji.php');

// Adds site styles to the WordPress editor
//require_once(get_template_directory().'/assets/functions/editor-styles.php');

// Related post function - no need to rely on plugins
// require_once(get_template_directory().'/assets/functions/related-posts.php');

// Use this as a template for custom post types
// require_once(get_template_directory().'/assets/functions/custom-post-type.php');

// Customize the WordPress login menu
// require_once(get_template_directory().'/assets/functions/login.php');

// Customize the WordPress admin
// require_once(get_template_directory().'/assets/functions/admin.php');

/**
 * Inline SVG from file
 *
 * @param string $svg_src Path to SVG file.
 * @return void.
 */
function excelerator_build_svg( $svg_src, $fallback = false) {
	if ( ! is_string( $svg_src ) || '' === $svg_src ) {
		return;
	}
	if ( ! is_readable( $svg_src ) ) {
		return;
	}
	$svg = file_get_contents( $svg_src );
	if ( $fallback ) {
		$png_src = str_replace( 'svg', 'png', $svg_src );
		$svg = excelerator_insert_svg_fallback( $svg, $png_src );
	}
	return $svg;
}

function excelerator_insert_svg_fallback( $svg, $png_src ) {
	// Add png fallback if available.
	if ( ! is_readable( $png_src ) ) {
		return $svg;
	} else {
		$png_insert = '<image src="' . $png_src . '" xlink:href=""></svg>';
		$svg = substr_replace( $svg, $png_insert, -6 );
		return $svg;
	}
}

/**
 * JavaScript Detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 */
function excelerator_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'excelerator_javascript_detection', 0 );
